import ExtendableError from 'es6-error';

export default class BreakError extends ExtendableError {
}
