import ExtendableError from 'es6-error';
 
export default class ContinueError extends ExtendableError {
}
